import java.util.Random;
/** 
 * This class is a subclass of Attraction, it contains methods and fields for a "dropper" attraction
 * it can simulate riding the ride and suing the ride  
 * Teacher: Heiser
 * Period: 6
 */  

public class Dropper extends Attraction{
	//Fields
	private int height; 
	private int splatterZoneRadius;
	//Constructor
	public Dropper(int speed, int seats, double ticketCost, String name, int closing, int height, int splatterZoneRadius) 
	{
		super(speed, seats, ticketCost, name, closing);
		this.height = height; 
		this.splatterZoneRadius = splatterZoneRadius;	
	}
	
	 /** 
     * This method is for converting the object of dropper into a string 
     * @author Nick Hamilton 
     * @param none
     * @return returns the specific fields this class has as well as the toString() method in the parent class
     */  
	public String toString()
	{
		return super.toString() + "\nHeight: " + height + "\nRadius of the SPLATTER ZONE: " + splatterZoneRadius;
	}
	
	 /** 
	 * @author Nick Hamilton 
     * @param none
     * This method is for simulating riding the dropper
     */  
	public void ride() 
	{
		Random rand = new Random(); 
		System.out.println("wooOOOOOOOooooooOOOOOOOH \n urp");
		System.out.println("You barf " + rand.nextInt(splatterZoneRadius) + " inches away from the base of the ride");
	}
	
	 /** 
     * This method is for suing the company 
     * @author Nicholas Hamilton 
     * @param how much money you want to sue for
     * @return returns both who you are suing and how much you are suing for 
     */  
	public void sue(float money) 
	{
		System.out.println("You sue " + super.getName() + " for $" + money + " in damages.");
	}

}
