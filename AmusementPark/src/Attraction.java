/**
 * This is the superclass for all the attractions
 * @author Ethan Pouncey, Nick Hamilton, Julian Young
 * Teacher: Heiser
 * Period: 6
 */
public class Attraction 
{	
	//Instance Fields
	private double rideSpeed;
	private int maxSeats;
	private double cost;
	private String name;
	private int closeHour;
	
	//Constructors
	public Attraction(double speed, int seats, double ticketCost, String aName, int closing)
	{
		rideSpeed = speed;
		maxSeats = seats;
		cost = ticketCost;
		name = aName;
		closeHour = closing;
	}
	
	/**
	 * This method will return the information of the ride as a String
	 * @param none
	 * @return String
	 * @author Ethan Pouncey
	 */
	public String toString()
	{
		return "Ride Information: " + name + "\nRide Speed: " + rideSpeed + " miles / hour \nTotal seats: " + maxSeats
				+ "\nTicket cost: $" + cost + "\nOperating Hours: Opening - " + closeHour + ":00";
	}
	
	/**
	 * This default method will simulate riding the attraction
	 * @param none
	 * @return none
	 * @author Nick Hamilton
	 */
	public void ride()
	{
		System.out.println("Wooooooh");
	}
	/**
	 * This method will return the speed of the ride
	 * @param none
	 * @return double
	 * @author Ethan Pouncey
	 */
	public double getSpeed()
	{
		return rideSpeed;
	}
	
	/**
	 * This method will return the maximum number of seats
	 * @param none
	 * @return int
	 * @author Ethan Pouncey
	 */
	public int getSeats() 
	{
		return maxSeats;
	}
	
	/**
	 * This method will return the cost of the ride
	 * @param none
	 * @return double
	 * @author Ethan Pouncey
	 */
	public double getCost()
	{
		return cost;
	}
	
	/**
	 * This method will return the ride name
	 * @param none
	 * @return String
	 * @author Ethan Pouncey
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * This method will return the closing time
	 * @param none
	 * @return int
	 * @author Ethan Pouncey
	 */
	public int getClosing()
	{
		return closeHour;
	}
	
	/**
	 * This method will update the ride's speed
	 * @param newSpeed
	 * @return none
	 * @author Ethan Pouncey
	 */
	public void updateSpeed(double newSpeed)
	{
		rideSpeed = newSpeed;
	}
}
