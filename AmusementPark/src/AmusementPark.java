import java.util.Scanner;

/** 
 * This class is a helper of Attraction, it simulates going to the amusement park 
 * 
 * @author Nick Hamilton, Ethan Pouncy, Julian Young    
 * Teacher: Heiser
 * Period: 6
 */  

public class AmusementPark 
{
	//Main method
	public static void main(String[] args)
	{
		//Initialize Variables
		Scanner console = new Scanner(System.in);
		boolean leave = false;
		
		/*
		 * I will leave the information of your attractions blank. 
		 * Please update them with whatever you deem necessary.
		 */
		MerryGoRound merry = new MerryGoRound(.2, 30, 3.99, "Merry-go-Down", 8, 284, "Skeleton", 90);
		Dropper dropper = new Dropper(20, 10, 5.50, "Dropper of Doom", 8, 500, 10);
		FerrisWheel ferriswheel = new FerrisWheel(1, 50, 2.99, "Ferris Fun Wheel", 8, 220, 5);
		
		System.out.println("Welcome to the Amusement Park!");
		while (!leave)
		{
			System.out.println("Please type the number of the ride you would like to go on."
							+ "\n 1: Ferris Wheel \n 2: Merry-go-Round \n 3: Dropper \n 4: Leave");
			switch(console.nextInt())
			{
				case 1: //ferris wheel simulation 
				{
					System.out.println("Come ride the most amazing type of wheel, the " + ferriswheel.getName() + "!");
					System.out.println(ferriswheel + "\n");
					System.out.println("The Wheel will turn in: ");
					for(int i = 8; i > 0; i--) 
					{
						System.out.println(i);
						try 
						{
							Thread.sleep(600);
						} 
						catch (InterruptedException e) 
						{
							e.printStackTrace();
						}
					}
					ferriswheel.ride();
					break;
				}
				case 2: //merry go round simulation 
				{
					merry.buySeats();
					System.out.println(merry.toString() + "\nThe ride will be beginning in 1 minute. Would you like to get on? ('1' = Yes, '2' = No)");
					if (console.nextInt() == 1)
					{
						merry.ride();
						System.out.println(merry.rideInfo() + "\n");
						merry.resetSpeed();
					}
					else;
					break;
				}
				case 3: //dropper simulation 
				{
					System.out.println("Welcome to " + dropper.getName() + "!");
					System.out.println(dropper + "\n");
					System.out.println("The ride is commencing in: ");
					for(int i = 5; i > 0; i--) {
						System.out.println(i);
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					dropper.ride();
					System.out.println("You don't feel so good. Would you like to sue? Y/N");
					if(console.next().equalsIgnoreCase("Y")) {
						System.out.println("How much would you like to sue for? ");
						dropper.sue(console.nextFloat());
					}
					break;

				}
				case 4: leave = true; //ends program
						break;
				default: System.out.println("This command was not accepted. Please try again.");
			}
		}
		System.out.println("Thank you for coming! Have a nice day!");
		console.close();
	}
}
