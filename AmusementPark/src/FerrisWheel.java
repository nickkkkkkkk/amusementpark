import java.util.Random;

public class FerrisWheel extends Attraction 
{
	//instance fields
	private int views; 
	private int timesaround;
	//constructor
	public FerrisWheel(int length, int seats, double ticketCost, String aName, int closing, int views, int timesaround)
	{
	super(length, seats, ticketCost, aName, closing); 
		this.views = views; 
		this.timesaround = timesaround;
	}
	/*Author: Julian Young
	 * This method states instance fields and the constructor. It sets up the program.
	 * @param: None
	 * Return: Returns specific fields
	 */
	public String toString()
	{
		return super.toString() + "\nviews: " + views;}
	/*Author: Julian Young
	 * This method runs the ride.
	 * @param: none
	 * Return: none
	 */
	public void ride() 
	{
		Random rand = new Random(); 
		System.out.println("You saw the whole park, it was very bright! you went around" + rand.nextInt( timesaround) + " times ");
		System.out.println("The park looked amazing.");
	}
	/*Author: Julian Young
	 * This method shows what happens after the ride.
	 * @param: None
	 * Return: None
	 */
}