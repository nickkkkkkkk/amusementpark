import java.util.Random;
/**
 * This class is a subclass of Attraction for a Merry-Go-Round attraction
 * @author Ethan Pouncey
 * Teacher: Heiser
 * Period: 6
 */
class MerryGoRound extends Attraction
{
	//Instance Fields
	private int casualties;
	private String animal;
	private int lengthSec;
	Random generator = new Random();
	private int seatsTaken;
	private double startingSpeed;
	
	//Constructor
	public MerryGoRound(double speed, int seats, double ticketCost, String aName, int closing, int deaths, String animalType, int seconds)
	{
		super(speed, seats, ticketCost, aName, closing);
		casualties = deaths;
		animal = animalType;
		lengthSec = seconds;
		startingSpeed = speed;
	}
	
	/**
	 * This method will decide how many seats have been bought out for the ride.
	 * @param none
	 * @return none
	 * @author Ethan Pouncey
	 */
	public void buySeats()
	{
		seatsTaken = generator.nextInt(getSeats() + 1);
	}
	
	/**
	 * This method will simulate the ride, stating one person's quote and calculating the speed and casualties.
	 * @param none
	 * @return none
	 * @author Ethan Pouncey
	 */
	public void ride()
	{
		if (seatsTaken != 0) 
		{
			System.out.println("Seat " + (seatsTaken) + ": \"Man, this ride is boring.\"");
		}
		for (int i = 0; i < lengthSec; i++)
		{
			if (generator.nextInt(3) + 1 == 1)
			{
				double newSpeed = getSpeed() + .5;
				updateSpeed(newSpeed);
			}		
		}
		for (int i = 0; i < seatsTaken; i++)
		{
			if (generator.nextInt(5) == 1)
			{
				casualties++;
			}	
		}
	}
	
	/**
	 * This method calls the super toString method and adds the additional information
	 * @param none
	 * @return String
	 * @author Ethan Pouncey
	 */
	public String toString()
	{
		return "\n" + super.toString() + "\nYou will be riding on " + animal + "s." 
				+ "\nOnly " + casualties + " people have died on this ride."
						+ "\n" + seatsTaken + " out of " + getSeats() + " seats have been filled.";
	}
	
	/**
	 * This method returns the max speed after a ride and the casualty count
	 * @param none
	 * @return String
	 * @author Ethan Pouncey
	 */
	public String rideInfo()
	{
		return "The ride hit a speed of " + getSpeed() + " miles / hour. "
				+ "Only " + casualties + " people have died on this ride.";
	}
	/**
	 * This method will reset the speed to default.
	 * @param none
	 * @return none
	 * @author Ethan Pouncey
	 */
	public void resetSpeed()
	{
		updateSpeed(startingSpeed);
	}
}
